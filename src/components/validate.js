export const validate = (data, type) => {
  const errors = {};

  

  if (!data.email) {
    errors.email = "Email required!";
  } else if (!/\S+@\S+\.\S+/.test(data.email)) {
    errors.email = "Your email is invalid!";
  } else {
    delete errors.email;
  }

  if (!data.password) {
    errors.password = "Password is required!";
  } else if (data.password.length < 6) {
    errors.password = "Password need to be atleast 6 character or more!";
  } else {
    delete errors.password;
  }

  if(type === "signup"){
    if (!data.name.trim()) {
      errors.name = "Username required!";
    } else {
      delete errors.name;
    }

    if (!data.confirmpassword) {
      errors.confirmpassword = "Confirm password";
    } else if (data.confirmpassword !== data.password) {
      errors.confirmpassword = "Password do not match!";
    } else {
      delete errors.confirmpassword;
    }
  
    if (data.isAccepted) {
      delete errors.isAccepted;
    } else {
      errors.isAccepted = "Please accept our regulations!";
    }


  }

  return errors;
};
